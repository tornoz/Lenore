package tornoz.org.lenore;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

/**
 * Created by gwenn on 4/15/18.
 */

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    protected EditText dateInput;

    public void setDateInput(EditText dateInput) {
        this.dateInput = dateInput;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        String[] dateData = dateInput.getText().toString().split("-");

        int year = Integer.parseInt(dateData[0]);
        int month = Integer.parseInt(dateData[1])-1;
        int day = Integer.parseInt(dateData[2]);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        String monthStr = String.format("%02d", month+1);
        String dayStr = String.format("%02d", day);

        dateInput.setText(year + "-" + monthStr + "-" + dayStr);
    }
}