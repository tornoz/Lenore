package tornoz.org.lenore;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Preferences preferences = new Preferences(getSharedPreferences("tornoz.org.lenore_preferences", Context.MODE_PRIVATE));

        String serverName = preferences.get("server_name_preference");
        String apiKey = preferences.get("api_key_preference");

        if (serverName.equals("") || apiKey.equals("") ) {
            Intent initIntent = new Intent(this, InitActivity.class);
            this.startActivity(initIntent);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        View contentView = findViewById(R.id.content);
        final String myKey = "";

        RequestQueue queue = Volley.newRequestQueue(this);
        ApiClient client = new ApiClient(queue, preferences);


        EditText dateTextInput = (EditText) contentView.findViewById(R.id.date);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateTextInput.setText(dateFormat.format(new Date()));
        final View formView = contentView.findViewById(R.id.transactionForm);


        Button dateButton = formView.findViewById(R.id.button_date);



        final DatePickerFragment newFragment = new DatePickerFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        newFragment.setDateInput(dateTextInput);
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newFragment.show(fragmentManager, "datePicker");
            }
        });
        Button sumbitButton = (Button) formView.findViewById(R.id.submit);
        sumbitButton.setOnClickListener(new SendTransactionListener(contentView, client));




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent initIntent = new Intent(this, InitActivity.class);
            this.startActivity(initIntent);
        }

        return super.onOptionsItemSelected(item);
    }
}
