package tornoz.org.lenore;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gwenn on 4/10/18.
 */

public class FireflyRequest extends JsonObjectRequest {

    protected String myKey = null;

    public FireflyRequest(String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, String myKey) {
        super(url, null, listener, errorListener);
        this.myKey = myKey;
    }

    public FireflyRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, String myKey) {
        super(method, url, jsonRequest, listener, errorListener);
        this.myKey = myKey;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();

        headers.put("Content-Type", "application/json");

        headers.put("Authorization", "Bearer " + this.myKey);

        return headers;
    }

    public void setKey(String key) {
        this.myKey = key;
    }
}
