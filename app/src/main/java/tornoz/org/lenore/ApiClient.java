package tornoz.org.lenore;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by gwenn on 4/10/18.
 */

public class ApiClient {

    public static final String URL_TRANSACTION = "/api/v1/transactions";

    protected RequestQueue $queue;

    protected RequestQueue queue;

    protected Preferences preferences;

    public ApiClient(RequestQueue queue, Preferences preferences) {
        this.queue = queue;
        this.preferences = preferences;
    }

    public void sendTransaction(
            JSONObject data,
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener
    ) {

        FireflyRequest stringRequest = new FireflyRequest(
                Request.Method.POST,
                this.preferences.get(Preferences.SERVER_NAME) + ApiClient.URL_TRANSACTION,
                data,
                listener,
                errorListener,
                this.preferences.get(Preferences.API_KEY)
        );

        this.queue.add(stringRequest);
    }

    public void getTransactions(
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener
    ) {

        FireflyRequest stringRequest = new FireflyRequest(
                this.preferences.get(Preferences.SERVER_NAME) + ApiClient.URL_TRANSACTION,
                listener,
                errorListener,
                this.preferences.get(Preferences.API_KEY)
        );

        this.queue.add(stringRequest);
    }
}
