package tornoz.org.lenore;

import android.content.SharedPreferences;

/**
 * Created by gwenn on 4/15/18.
 */

public class Preferences {

    public static final String API_KEY = "api_key_preference";
    public static final String SERVER_NAME = "server_name_preference";

    protected SharedPreferences preferences;

    public Preferences(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public void set(String key, String value) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String get(String key) {
        return this.preferences.getString(key, "");
    }


}
