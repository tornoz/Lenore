package tornoz.org.lenore;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SendTransactionListener implements View.OnClickListener {

    protected View contentView;
    protected ApiClient client;


    public SendTransactionListener(View activityView, ApiClient client) {
        this.client = client;
        this.contentView = activityView;
    }

    @Override
    public void onClick(View v) {

//                URLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.addRequestProperty("client_id", "3");
//                conn.addRequestProperty("client_secret", "l7tx1qW1j2ETi6ey80y2x1eIC6bqfEqBw2FpdUWF");
//                conn.setRequestProperty("Authorization", "OAuth " + myKey);
        //Get form data

        final View formView = contentView.findViewById(R.id.transactionForm);

//        String description = ((EditText)formView.findViewById(R.id.description)).getText().toString();

        String type = "withdrawal";
        String currency_code = "EUR";
        String description = ((EditText)formView.findViewById(R.id.description)).getText().toString();
        String date = ((EditText)formView.findViewById(R.id.date)).getText().toString();
        String amount = ((EditText)formView.findViewById(R.id.amount)).getText().toString();
        String source_id = ((EditText)formView.findViewById(R.id.source_id)).getText().toString();
        String destination_name= ((EditText)formView.findViewById(R.id.destination_name)).getText().toString();

        JSONObject object = new JSONObject();
        JSONArray transactionArray = new JSONArray();

        JSONObject transaction = new JSONObject();
        try {

            object.put("type", type);
            object.put("description", description);
            object.put("date", date);
            transaction.put("source_id", source_id);
            transaction.put("destination_name", destination_name);
            transaction.put("currency_code", currency_code);
            transaction.put("description", description+ "aaa");
            transaction.put("amount", amount);
            transactionArray.put(transaction);
            object.put("transactions", transactionArray);

        } catch (JSONException e) {
            System.err.println("oh no :( error");
            System.err.println(e.getMessage());
        }

        System.out.println(object.toString());

//
        client.sendTransaction(
                object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display the first 500 characters of the response string.
                        Snackbar mySnackbar = Snackbar.make(formView, "Error: " + response.toString(), Snackbar.LENGTH_LONG);
                        if (response.has("data")) {
                            mySnackbar = Snackbar.make(formView, "Success", Snackbar.LENGTH_LONG);
                        }
                        mySnackbar.show();

                        System.out.println("Response:");
                        System.out.println(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar mySnackbar = Snackbar.make(formView, "Error :(", Snackbar.LENGTH_LONG);
                        mySnackbar.show();

                    }
                });

//        client.getTransactions(
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        // Display the first 500 characters of the response string.
//
//
//                        textView.setText("Response is: "+
//                                response.toString().substring(0,Math.min(response.length(), 500)));
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        textView.setText("That didn't work! " + error.getMessage());
//                        System.err.println(error.getMessage());
//
//                    }
//                });
    }
}
